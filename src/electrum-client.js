const Client = require('./client')

const { createMethods, namespaces } = require('../create-methods')

class ElectrumClient extends Client {
  constructor (port, host, protocol, options) {
    super(port, host, protocol, options)

    namespaces.forEach(namespace =>
      (this[namespace] = createMethods(namespace, this.request.bind(this))))
  }

  onClose () {
    super.onClose()
    const list = [
      'server.peers.subscribe',
      'blockchain.numblocks.subscribe',
      'blockchain.headers.subscribe',
      'blockchain.address.subscribe'
    ]
    list.forEach(event => this.subscribe.removeAllListeners(event))
  }
}

module.exports = ElectrumClient
