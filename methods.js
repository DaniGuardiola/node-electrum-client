/* HOW TO AUTOMATICALLY GENERATE THIS LIST

1. Open Electrum X protocol methods documentation on a modern browser:
https://electrumx.readthedocs.io/en/latest/protocol-methods.html

2. Open the JavaScript console through the developer tools

3. Copy and paste this snippet of code and execute it:

const methodStrings = Array.from(document
  .querySelectorAll('#protocol-methods > .section > h2'))
  .map(el => el.textContent.replace(/¶/g, '').trim())

const output = {}

methodStrings.forEach(s => {
  const parts = s.split('.')
  const namespace = parts[0]

  if (!output[namespace]) output[namespace] = []

  if (parts.length === 2) output[namespace].push(parts[1])
  else {
    const subNamespace = parts[1]
    const method = parts[2]

    const index = output[namespace].findIndex(item => item[0] === subNamespace)

    if (index > -1) output[namespace][index].push(method)
    else output[namespace].push([subNamespace, method])
  }
})

console.log(JSON.stringify(output, null, 2))

4. Copy the output from the console and paste it below,
adding 'module.exports = ' before the object

*/

module.exports = {
  'blockchain': [
    [
      'address',
      'get_balance',
      'get_history',
      'get_mempool',
      'listunspent',
      'subscribe'
    ],
    [
      'block',
      'get_header',
      'get_chunk',
      'headers'
    ],
    'estimatefee',
    [
      'headers',
      'subscribe'
    ],
    [
      'numblocks',
      'subscribe'
    ],
    'relayfee',
    [
      'scripthash',
      'get_balance',
      'get_history',
      'get_mempool',
      'listunspent',
      'subscribe'
    ],
    [
      'transaction',
      'broadcast',
      'get',
      'get_merkle'
    ],
    [
      'utxo',
      'get_address'
    ]
  ],
  'mempool': [
    'get_fee_histogram'
  ],
  'server': [
    'add_peer',
    'banner',
    'donation_address',
    'features',
    [
      'peers',
      'subscribe'
    ],
    'ping',
    'version'
  ]
}
