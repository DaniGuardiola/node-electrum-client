const methods = require('./methods')

const camelCase = s => s.replace(/_([a-z])/g, (g) => g[1].toUpperCase())

const createMethod = (method, request) => function () { return request(method, Array.from(arguments)) }

const createMethods = (namespace, request) => {
  if (!methods[namespace]) throw new Error(`Method namespace '${namespace}' doesn't exist`)
  const output = {}
  methods[namespace].forEach(data => {
    if (data.constructor === Array) {
      const subNamespace = data.shift()
      output[subNamespace] = {}
      data.forEach(method =>
        (output[subNamespace][camelCase(method)] = createMethod(`${namespace}.${subNamespace}.${method}`, request)))
    } else output[camelCase(data)] = createMethod(`${namespace}.${data}`, request)
  })
  return output
}

module.exports = { createMethods, namespaces: Object.keys(methods) }
